import React, { useState, useEffect } from 'react'

export default function Slider({ fotos }) {
    const [images, setImages] = useState(fotos)
    const [position, setPosition] = useState(0)
    useEffect(() => {
        const timeoutID = setTimeout(toonVolgendeFoto, 2000)
        return () => {
            clearTimeout(timeoutID)
        }
    }, [position])

    const toonVolgendeFoto = () => {
        if (position === 2) {
            setPosition(0)
        } else {
            setPosition(position + 1)
        }
    }
    return (
        <div className="slider">
            {
                images && images.map((img, i) => <img key={i} src={img} className={
                    i === position ? "show" : "hide"
                } />)
            }
        </div>
    )
}


