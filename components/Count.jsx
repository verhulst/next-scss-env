import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { increment, decrement, setTo } from "../data/count";
export default () => {
  const count = useSelector(state => state.count);
  const dispatch = useDispatch();
  return (
    <div>
      <h1>{count.val}</h1>
      <button
        onClick={() => {
          dispatch(increment());
        }}
      >
        +
      </button>
      <button
        onClick={() => {
          dispatch(decrement());
        }}
      >
        -
      </button>
      <button
        onClick={() => {
          dispatch(setTo(5));
        }}
      >
        Set to Five
      </button>
    </div>
  );
};
