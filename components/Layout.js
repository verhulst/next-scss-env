import React from 'react'
import Link from 'next/link'
import Header from './Header'
import Footer from './Footer'
import Head from 'next/head'
import Menu from './Menu'
export default function Layout({ children, title, description, footer }) {
    return (
        <>
            <Head>
                <title>{title}</title>
                <link rel="icon" href="/favicon.ico" />
                <meta name="description" content={description} />
            </Head>
            <Header title={title} />
            <Menu />
            <hr />
            <nav>
                <ul>
                    <li><Link href="/"><a>home</a></Link></li>
                    <li><Link href="/about"><a>about</a></Link></li>
                    <li><Link href="/friends"><a>friends</a></Link></li>
                    <li><Link href="/movies"><a>movies</a></Link></li>
                </ul>
            </nav>
            <main>
                {children}
            </main>
            <Footer footer={footer} />
            {/* <style jsx>{`
                p{
                color:blue;
                }
            `}</style>
            <style jsx global>{`
                html,
                body {
                padding: 0;
                margin: 0;
                font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
                    Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
                    sans-serif;
                }

                * {
                box-sizing: border-box;
                }
            `}</style> */}
        </>
    )
}
