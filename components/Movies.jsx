import React from "react";
import MovieForm from "./MovieForm";
import { useSelector } from "react-redux";
export default () => {
  const {
    loading,
    error,
    data,
    progress: { loaded, total }
  } = useSelector(state => state.movies);
  return (
    <div>
      <MovieForm />
      {loading && <p>Loading...</p>}
      {loaded !== 0 && (
        <p>
          Loading {loaded}/{total} movies...
        </p>
      )}
      {error !== "" && <p>{error}</p>}
      {data.length !== 0 && (
        <ol>
          {data.map(({ imdbID, Title, Year }) => (
            <li key={imdbID}>
              {Title} ({Year})
            </li>
          ))}
        </ol>
      )}
    </div>
  );
};
