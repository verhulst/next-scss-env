import React from 'react'
import globalContext from './globalData';

export default function Footer() {
    const data = useContext(globalContext)
    return (
        <div>
            {data.data}
        </div>
    )
}

