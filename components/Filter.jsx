import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { toggleEven, toggleAdult } from "../data/filter";
export default () => {
  const filterData = useSelector(state => state.filter);
  const dispatch = useDispatch();
  const evenHandler = e => {
    dispatch(toggleEven());
  };
  const adultHandler = e => {
    dispatch(toggleAdult());
  };
  return (
    <div>
      <input
        type="checkbox"
        checked={filterData.isEven}
        onChange={evenHandler}
      />{" "}
      Even
      <input
        type="checkbox"
        checked={filterData.isAdult}
        onChange={adultHandler}
      />{" "}
      Adult
    </div>
  );
};
