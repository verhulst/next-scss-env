import React from 'react'

export default function Menu() {
    return (
        <nav className="navigatie">
            <a href="/"><img src="http://dummyimage.com/200x100?text=logo" alt=""/></a>
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">Services</a>
                    <ul>
                        <li><a href="#">Services 1</a></li>
                        <li><a href="#">Services 2</a></li>
                        <li><a href="#">Services 3</a></li>
                        <li><a href="#">Services 4</a></li>
                    </ul>
                </li>
                <li><a href="#">Products</a>
                </li>
                <li><a href="#">Contact</a></li>
            </ul> 
        </nav>
    )
}
