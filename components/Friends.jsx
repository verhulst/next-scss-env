import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { addFriend, removeFriend } from "../data/friends";
import { useField } from "../hooks";

export default () => {
  const data = useSelector(state => ({
    friends: state.friends,
    filter: state.filter
  }));
  const dispatch = useDispatch();
  const nameField = useField();
  const ageField = useField();
  const clickHandler = e => {
    dispatch(addFriend(nameField.value, ageField.value));
  };
  const {
    friends: { past, present, future },
    filter
  } = data;
  const removeHandler = id => e => {
    dispatch(removeFriend(id));
  };
  return (
    <div>
      Name: <input type="text" {...nameField} />
      Age: <input type="text" {...ageField} />
      <button onClick={clickHandler}>add Friend</button>
      <ul>
        {present
          .filter(friend => (filter.isEven ? friend.age % 2 === 0 : true))
          .filter(friend => (filter.isAdult ? friend.age >= 18 : true))
          .map(friend => (
            <li key={friend.id}>
              {friend.fn} ({friend.age}){" "}
              <button onClick={removeHandler(friend.id)}>X</button>
            </li>
          ))}
      </ul>
      {past.length !== 0 && (
        <button onClick={e => dispatch({ type: "UNDO_FRIENDS" })}>undo</button>
      )}
      {future.length !== 0 && (
        <button onClick={e => dispatch({ type: "REDO_FRIENDS" })}>redo</button>
      )}
    </div>
  );
};
