const footerText = async () => {
    const footerText = await new Promise((resolve, reject) => {
        setTimeout(() => resolve("Tekst voor in de footer"), 0)
    })
    return footerText
}

module.exports = footerText;