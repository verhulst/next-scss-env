import { v4 as uuid } from "uuid";

/*****************/
/* INITIAL STATE */
/*****************/
const initialState = [
  {
    id: uuid(),
    fn: "David",
    age: 38
  },
  {
    id: uuid(),
    fn: "Els",
    age: 29
  },
  {
    id: uuid(),
    fn: "Jos",
    age: 16
  },
  {
    id: uuid(),
    fn: "Pieter",
    age: 22
  },
  {
    id: uuid(),
    fn: "Jordan",
    age: 24
  },
  {
    id: uuid(),
    fn: "Romelu",
    age: 19
  },
  {
    id: uuid(),
    fn: "Hannelore",
    age: 63
  }
];

/*********/
/* TYPES */
/*********/
const ADD_FRIEND = "ADD_FRIEND";
const REMOVE_FRIEND = "REMOVE_FRIEND";
const UNDO_FRIENDS = "UNDO_FRIENDS";
const REDO_FRIENDS = "REDO_FRIENDS";

/*******************/

/* ACTION CREATORS */
/*******************/
export const addFriend = (fn, age) => ({
  type: ADD_FRIEND,
  payload: {
    id: uuid(),
    fn,
    age
  }
});
export const removeFriend = id => ({
  type: REMOVE_FRIEND,
  payload: id
});

/********************/
/* UNDO-REDO CONFIG */
/********************/
export const undoFriendsConfig = {
  undoType: UNDO_FRIENDS,
  redoType: REDO_FRIENDS
};

/***********/
/* REDUCER */
/***********/
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_FRIEND:
      return [...state, payload];
    case REMOVE_FRIEND:
      return state.filter(friend => friend.id !== payload);
    default:
      return state;
  }
};
