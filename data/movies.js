import axios from "axios";

/*****************/
/* INITIAL STATE */
/*****************/
const initialState = {
  loading: false,
  error: "",
  data: [],
  progress: {
    page: 0,
    pages: 0,
    total: 0,
    loaded: 0
  }
};

/*********/
/* TYPES */
/*********/
const FETCH_MOVIES_START = "FETCH_MOVIES_START";
const FETCH_MOVIES_SUCCESS = "FETCH_MOVIES_SUCCESS";
const FETCH_MOVIES_ERROR = "FETCH_MOVIES_ERROR";
const FETCH_NEXT_MOVIES_START = "FETCH_NEXT_MOVIES_START";
const FETCH_NEXT_MOVIES_SUCCESS = "FETCH_NEXT_MOVIES_SUCCESS";

/*******************/
/* ACTION CREATORS */
/*******************/
export const getNextMovies = str => (dispatch, getState) => {
  const {
    movies: {
      progress: { page, pages }
    }
  } = getState();
  const pageToLoad = page + 1;
  dispatch(loadNextMovies(pageToLoad));
  axios
    .get(
      `https://www.omdbapi.com/?apikey=2e3b4604&type=movie&page=${pageToLoad}&s=${str}`
    )
    .then(response => {
      dispatch(setNextMovies(response.data.Search));
      if (pageToLoad !== pages) {
        dispatch(getNextMovies(str));
      }
    })
    .catch(error => dispatch(setError("Api endpoint could not be reached")));
};
export const getMovies = str => dispatch => {
  dispatch(loadMovies());
  axios
    .get(`https://www.omdbapi.com/?apikey=2e3b4604&type=movie&page=1&s=${str}`)
    .then(response => {
      if (response.data.Response === "False") {
        dispatch(setError("No movies found"));
      } else {
        dispatch(
          setMovies(
            response.data.Search,
            parseInt(response.data.totalResults, 10)
          )
        );
        if (
          parseInt(response.data.totalResults, 10) > response.data.Search.length
        ) {
          dispatch(getNextMovies(str));
        }
      }
    })
    .catch(error => dispatch(setError("Api endpoint could not be reached")));
};
export const loadMovies = () => ({ type: FETCH_MOVIES_START });
export const setMovies = (movies, total) => ({
  type: FETCH_MOVIES_SUCCESS,
  payload: {
    movies,
    total
  }
});
export const loadNextMovies = page => ({
  type: FETCH_NEXT_MOVIES_START,
  payload: page
});
export const setNextMovies = movies => ({
  type: FETCH_NEXT_MOVIES_SUCCESS,
  payload: movies
});
export const setError = msg => ({ type: FETCH_MOVIES_ERROR, payload: msg });

/***********/
/* REDUCER */
/***********/
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_MOVIES_START:
      return {
        ...state,
        loading: true,
        error: "",
        progress: {
          page: 1,
          pages: 0,
          loaded: 0,
          total: 0
        }
      };
    case FETCH_MOVIES_SUCCESS:
      return {
        ...state,
        loading: false,
        data: payload.movies,
        progress: {
          ...state.progress,
          loaded: payload.movies.length,
          pages: Math.ceil(payload.total / payload.movies.length),
          total: payload.total
        }
      };
    case FETCH_NEXT_MOVIES_START:
      return {
        ...state,
        error: "",
        progress: {
          ...state.progress,
          page: payload
        }
      };
    case FETCH_NEXT_MOVIES_SUCCESS:
      return {
        ...state,
        loading: false,
        data: [...state.data, ...payload],
        progress: {
          ...state.progress,
          loaded: state.progress.loaded + payload.length
        }
      };
    case FETCH_MOVIES_ERROR:
      return {
        ...state,
        loading: false,
        error: payload
      };
    default:
      return state;
  }
};
