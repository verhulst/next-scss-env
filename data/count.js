/*****************/
/* INITIAL STATE */
/*****************/
const initialState = { val: 0 };

/*********/
/* TYPES */
/*********/
const INCREMENT = "INCREMENT";
const DECREMENT = "DECREMENT";
const SETTO = "SETTO";
/*******************/
/* ACTION CREATORS */
/*******************/
export const increment = () => ({
  type: INCREMENT
});
export const decrement = () => ({
  type: DECREMENT
});
export const setTo = val => ({
  type: SETTO,
  payload: val
});

/***********/
/* REDUCER */
/***********/
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case INCREMENT:
      return { val: state.val + 1 }
    case DECREMENT:
      return { val: state.val - 1 }
    case SETTO:
      return { val: payload }
    default:
      return state;
  }
};
