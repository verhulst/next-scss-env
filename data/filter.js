/*****************/
/* INITIAL STATE */
/*****************/
const initialState = {
  isEven: false,
  isAdult: false
};

/*********/
/* TYPES */
/*********/
const TOGGLE_EVEN = "TOGGLE_EVEN";
const TOGGLE_ADULT = "TOGGLE_ADULT";

/*******************/
/* ACTION CREATORS */
/*******************/
export const toggleEven = () => ({
  type: TOGGLE_EVEN
});
export const toggleAdult = () => ({
  type: TOGGLE_ADULT
});

/***********/
/* REDUCER */
/***********/
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case TOGGLE_EVEN:
      return { ...state, isEven: !state.isEven };
    case TOGGLE_ADULT:
      return { ...state, isAdult: !state.isAdult };
    default:
      return state;
  }
};
