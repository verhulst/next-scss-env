import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import { createStore, applyMiddleware, combineReducers } from "redux";
import { createWrapper } from 'next-redux-wrapper'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from "redux-thunk";
//import logger from "redux-logger";
import undoable from "redux-undo";
import friendsReducer, { undoFriendsConfig } from "./friends";
import countReducer from "./count";
import filterReducer from "./filter";
import moviesReducer from "./movies";

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, combineReducers({
  friends: undoable(friendsReducer, undoFriendsConfig),
  count: countReducer,
  filter: filterReducer,
  movies: moviesReducer
}))


export const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(thunk)))
export const persistor = persistStore(store)

const makeStore = () => store;
export default createWrapper(makeStore)