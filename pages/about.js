import React from 'react'
import Layout from '../components/Layout'
import Count from '../components/Count'

export default function about(props) {
    return (
        <Layout title={props.title} description={props.description} footer={props.footer}>
            {/* <h1>next met scss en .env support</h1>
            <h2>public env variabel: {process.env.NEXT_PUBLIC_ENVIRONMENT}</h2> */}
            <hr />
            <Count />
            <div className="grid">
                <div>
                    <img src="http://dummyimage.com/200x200/000/fff&text=fotoke" alt="" />
                    <p></p>
                    <button></button>
                </div>
                <div>
                    <img src="http://dummyimage.com/200x200/000/fff&text=fotoke" alt="" />
                    <p></p>
                    <button></button>
                </div>
                <div>
                    <img src="http://dummyimage.com/200x200/000/fff&text=fotoke" alt="" />
                    <p></p>
                    <button></button>
                </div>
                <div>
                    <img src="http://dummyimage.com/200x200/000/fff&text=fotoke" alt="" />
                    <p></p>
                    <button></button>
                </div>
                <div>
                    <img src="http://dummyimage.com/200x200/000/fff&text=fotoke" alt="" />
                    <p></p>
                    <button></button>
                </div>
                <div>
                    <img src="http://dummyimage.com/200x200/000/fff&text=fotoke" alt="" />
                    <p></p>
                    <button></button>
                </div>
                <div>
                    <img src="http://dummyimage.com/200x200/000/fff&text=fotoke" alt="" />
                    <p></p>
                    <button></button>
                </div>
                <div>
                    <img src="http://dummyimage.com/200x200/000/fff&text=fotoke" alt="" />
                    <p></p>
                    <button></button>
                </div>
                <div>
                    <img src="http://dummyimage.com/200x200/000/fff&text=fotoke" alt="" />
                    <p></p>
                    <button></button>
                </div>
                <div>
                    <img src="http://dummyimage.com/200x200/000/fff&text=fotoke" alt="" />
                    <p></p>
                    <button></button>
                </div>
            </div>


        </Layout>
    )
}

export const getStaticProps = async () => {
    //env var enkel serverside available
    //console.log(process.env.API_KEY);
    const getFooterStuff = require('../helpers')

    const titleText = await Promise.resolve("ABOUT")
    const descriptionText = await Promise.resolve("Tekst voor in de description op de about")
    return {
        props: {
            footer: await getFooterStuff(),
            title: titleText,
            description: descriptionText
        }
    }
}