import App from 'next/app'
import '../css/style.scss';
import wrapper, { store, persistor } from '../data'
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux'

function MyApp(props) {
    const { Component, pageProps } = props
    return (
        <Provider store={store}>
            <PersistGate persistor={persistor} loading={null}>
                <Component {...pageProps} />
            </PersistGate>
        </Provider>
    )
}

export default wrapper.withRedux(MyApp)