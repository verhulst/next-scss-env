import React from 'react'
import Movies from '../components/Movies'
import Count from '../components/Count'
import Layout from '../components/Layout'

export default function movies(props) {
    return (
        <Layout title={props.title} description={props.description} footer={props.footer}>
            <Movies />
            <hr />
            <Count />
        </Layout>
    )
}

export const getStaticProps = async () => {
    // //env var enkel serverside available
    // console.log(process.env.API_KEY);
    const getFooterStuff = require('../helpers')

    const titleText = "MOVIES";
    const descriptionText = await Promise.resolve("Tekst voor in de description op de home")
    return {
        props: {
            footer: await getFooterStuff(),
            title: titleText,
            description: descriptionText
        }
    }
}