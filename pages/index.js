import React, { useEffect } from 'react'
import Layout from '../components/Layout'
import Count from '../components/Count'
// import wrapper from '../data'
import { useDispatch } from 'react-redux'
import { parseCookies, setCookie, destroyCookie } from 'nookies'

export default function Home(props) {
  const dispatch = useDispatch();
  useEffect(() => {
    if (props.serverCount) {
      dispatch({
        type: "SETTO",
        payload: props.serverCount
      })
    }
  }, [])
  return (
    <Layout title={props.title} description={props.description} footer={props.footer}>
      <Count />
    </Layout>
  )
}
// export const getStaticProps = async () => {
//   //env var enkel serverside available
//   console.log(process.env.API_KEY);
//   const getFooterStuff = require('../helpers')

//   const titleText = "HOME";
//   const descriptionText = await Promise.resolve("Tekst voor in de description op de home")
//   return {
//     props: {
//       footer: await getFooterStuff(),
//       title: titleText,
//       description: descriptionText
//     }
//   }
// }


//NPM RUN DEV => elke keer als je de / (home) opvraagt => 2sec
//NPM RUN BUILD => NPM RUN START => statische gegenereerde paginas testen => onmiddellijk
export const getServerSideProps = async (ctx) => {
  const cookies = parseCookies(ctx);
  let serverCount = null;

  if (cookies.fetched !== "1") {
    serverCount = await new Promise((resolve, reject) => {
      setTimeout(() => resolve(666), 0)
    });
    setCookie(ctx, 'fetched', '1', {
      maxAge: 30 * 24 * 60 * 60,
      path: '/',
    })
  }


  const getFooterStuff = require('../helpers')
  const titleText = "HOME";
  const descriptionText = await Promise.resolve("Tekst voor in de description op de home")
  return {
    props: {
      footer: await getFooterStuff(),
      title: titleText,
      description: descriptionText,
      serverCount
    }
  }
}

