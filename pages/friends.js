import React from 'react'
import Friends from '../components/Friends'
import Filter from '../components/Filter'
import Count from '../components/Count'
import Layout from '../components/Layout'
import Slider from '../components/Slider'
import axios from 'axios'
// import Axios from 'axios'
export default function friends(props) {
    return (
        <Layout title={props.title} description={props.description} footer={props.footer}>
            <Friends />
            <Slider fotos={props.fotos} />
            <Filter />
            <hr />
            <Count />
        </Layout>
    )
}


export const getStaticProps = async () => {
    // //env var enkel serverside available
    // console.log(process.env.API_KEY);
    const getFooterStuff = require('../helpers')

    const titleText = "FRIENDS";
    const descriptionText = await Promise.resolve("Tekst voor in de description op de home")
    //volgende regel moet dus een axios call zijn die alle foto's ophaalt
    const response = await Promise.resolve(["http://dummyimage.com/800x600/000/fff&text=fotoke", "http://dummyimage.com/800x600/000/fff&text=fotoke2", "http://dummyimage.com/800x600/000/fff&text=fotoke3"])
    return {
        props: {
            footer: await getFooterStuff(),
            title: titleText,
            description: descriptionText,
            fotos: response
        }
    }
}



