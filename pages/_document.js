import Document, { Html, Head, Main, NextScript } from 'next/document'
// import globalContext from '../components/globalData';

class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        //console.log("nu")
        return {
            ...initialProps
        }
    }

    render() {
        //console.log(this.props.globalData)
        return (
            <Html>
                <Head />
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>

        )
    }
}

export default MyDocument