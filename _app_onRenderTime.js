import React from 'react';
import App from 'next/app';
import '../css/style.scss';
import axios from "axios";
import globalContext from '../components/globalData';

class MyApp extends App {
    state = {
        globalData: {
            loading: false,
            data: {},
            error: false
        }
    }
    componentDidMount() {
        this.setState({
            ...this.state,
            globalData: {
                ...this.state.globalData,
                loading: true
            }
        });
        axios.get("http://omdbapi.com/?apikey=2e3b4604&t=titanic").then(resp => {
            this.setState({
                ...this.state,
                globalData: {
                    ...this.state.globalData,
                    data: resp.data,
                    loading: false
                }
            });
        }).catch(console.log)
    }
    render() {
        const { Component, pageProps } = this.props;
        return (
            <globalContext.Provider value={this.state.globalData}>
                <Component {...pageProps} />
            </globalContext.Provider>
        );
    }
}


export default MyApp;